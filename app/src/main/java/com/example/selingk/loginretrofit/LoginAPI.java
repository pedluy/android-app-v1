package com.example.selingk.loginretrofit;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * Created by Selin GÖK on 9.8.2016.
 */
public interface LoginAPI {
    /*Retrofit get annotation with our URL
       And our method that will return us the list ob Book
    */
    @POST("/login")
    public void login(@Body UserModel user, Callback<ResponseModel> callback);

    @POST("/add")
    public void addUser(@Body UserModel user, Callback<ResponseModel> callback);

    @GET("/accountcontrol/{username}")
    public void accountcontrol(@Path("username") String user, Callback<ResponseModel> callback);



}
