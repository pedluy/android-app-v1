package com.example.selingk.loginretrofit;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;



import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends AppCompatActivity {

    private Button loginButton;
    private Button registerButton;
    private EditText usernameTV;
    private EditText passwordTV;
    UserModel user = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loginButton = (Button) findViewById(R.id.loginButton);
        registerButton = (Button) findViewById(R.id.registerButton);
        usernameTV = (EditText) findViewById(R.id.usernameEditText);
        passwordTV = (EditText) findViewById(R.id.passwordEditText);

        usernameTV.setText("");
        passwordTV.setText("");

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://192.168.0.19:8080/SpringExample/rest/loginservice")
                .build();


        final LoginAPI service = restAdapter.create(LoginAPI.class);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                user=new UserModel();
                user.setUsername(usernameTV.getText().toString());
                user.setPassword(passwordTV.getText().toString());

                service.login(user, new Callback<ResponseModel>() {
                    @Override
                    public void success(ResponseModel responseModel, Response response) {
                        if(responseModel.getStatus() == 200){

                            Intent intend = new Intent("com.example.selingk.loginretrofit.User");
                            intend.putExtra("username",usernameTV.getText().toString());
                            intend.putExtra("password",passwordTV.getText().toString());
                            startActivity(intend);
                            Message(responseModel.getMessage());
                        ;}
                        else
                            Message(responseModel.getMessage());
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Message(error.getMessage());
                    }
                });

            }
        });

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                user = new UserModel();
                user.setUsername(usernameTV.getText().toString());
                user.setPassword(passwordTV.getText().toString());

                service.accountcontrol(user.getUsername(), new Callback<ResponseModel>() {
                    @Override
                    public void success(ResponseModel responseModel, Response response) {
                        if(responseModel.getStatus() == 200){
                            service.addUser(user, new Callback<ResponseModel>() {
                                @Override
                                public void success(ResponseModel responseModel, Response response) {
                                    Message(responseModel.getMessage());
                                }

                                @Override
                                public void failure(RetrofitError error) {
                                    Message(error.getMessage());
                                }
                            });

                            usernameTV.setText("");
                            passwordTV.setText("");
                        }
                        else{
                            Message(responseModel.getMessage());
                            usernameTV.setText("");
                            passwordTV.setText("");
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Message(error.getMessage());
                    }
                });


            }
        });



        }

    private void Message(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }



    }

