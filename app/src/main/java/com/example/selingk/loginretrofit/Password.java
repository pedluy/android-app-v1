package com.example.selingk.loginretrofit;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class Password extends AppCompatActivity {

    TextView oldPassTV;
    TextView newPassTV;
    EditText oldPassET;
    EditText newPassET;
    EditText newPassRetypeET;
    Button changeButton;

    UserModel user;

    RestAdapter restAdapter = new RestAdapter.Builder()
            .setEndpoint("http://192.168.0.19:8080/SpringExample/rest/user")
            .build();

    final UserAPI service = restAdapter.create(UserAPI.class);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password);

        oldPassTV = (TextView) findViewById(R.id.oldPassTV);
        newPassTV = (TextView) findViewById(R.id.newPassTV);
        oldPassET = (EditText) findViewById(R.id.oldPassEditText);
        newPassET = (EditText) findViewById(R.id.newPassEditText);
        newPassRetypeET = (EditText) findViewById(R.id.newPassRetypeEditText);
        changeButton = (Button) findViewById(R.id.changeButton);

        changeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle extras = getIntent().getExtras();
                String username = extras.getString("username");
                String password = extras.getString("password");
                user = new UserModel();
                user.setUsername(username);
                user.setPassword(password);

                if(!password.equals(oldPassET.getText().toString()))
                    Message("Check your password!");
                else if("".equals(oldPassET.getText().toString()))
                    Message("Old password can not be null!");
                else {
                    if(!newPassET.getText().toString().equals(newPassRetypeET.getText().toString()))
                        Message("Passwords do not match!");
                    else{
                        service.updatePassword(user, newPassET.getText().toString(), new Callback<ResponseModel>() {
                            @Override
                            public void success(ResponseModel responseModel, Response response) {
                                Message(responseModel.getMessage());
                                Password.this.finish();
                            }

                            @Override
                            public void failure(RetrofitError error) {
                                Message(error.getMessage());
                            }
                        });
                    }



                }



                //service.updatePassword();
            }
        });

    }

    private void Message(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
