package com.example.selingk.loginretrofit;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class User extends AppCompatActivity {

    private Button deteteButton;
    private Button changePassButton;

   /* Bundle extras = getIntent().getExtras();
    String username = extras.getString("username");
    String password = extras.getString("password");*/


    private UserModel user = new UserModel();
    final Context context = this;

    RestAdapter restAdapter = new RestAdapter.Builder()
            .setEndpoint("http://192.168.0.19:8080/SpringExample/rest/user")
            .build();

    final UserAPI service = restAdapter.create(UserAPI.class);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        deteteButton = (Button) findViewById(R.id.deleteButton);
        changePassButton = (Button) findViewById(R.id.changePassButton);

        changePassButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle extras = getIntent().getExtras();
                String username = extras.getString("username");
                String password = extras.getString("password");
                Intent intend = new Intent("com.example.selingk.loginretrofit.Password");
                intend.putExtra("username",username);
                intend.putExtra("password",password);
                startActivity(intend);
            }
        });

        deteteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle extras = getIntent().getExtras();
                    String username = extras.getString("username");
                    String password = extras.getString("password");
                    user.setUsername(username);
                    user.setPassword(password);
                    /**
                     * AlertDialog : i used for asking "Are you sure?"
                     */
                    AlertDialog.Builder alert = new AlertDialog.Builder(context);
                    alert.setTitle("WARNING");
                    alert
                            .setMessage("Are you sure?")

                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    service.deleteAccount(user, new Callback<ResponseModel>() {
                                        @Override
                                        public void success(ResponseModel responseModel, Response response) {
                                            Message(responseModel.getMessage());
                                        }

                                        @Override
                                        public void failure(RetrofitError error) {
                                            Message(error.getMessage());
                                        }
                                    });
                                    User.this.finish();
                                    //Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                    //startActivity(intent);
                                }

                            })
                            .setNegativeButton("No",new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alertDialog = alert.create();
                    alertDialog.show();

                }

        });

    }


    private void Message(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
