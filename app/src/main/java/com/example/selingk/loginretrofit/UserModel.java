package com.example.selingk.loginretrofit;

/**
 * Created by Selin GÖK on 9.8.2016.
 */
public class UserModel {
    private String username;
    private String password;
    private int id;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
